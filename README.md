#  Basic Crawler

## Requirements

This project is Python 3.6.

Uses:

* requests
* beautifulsoup4

## Setup

```
$ virtualenv -p python3 env
$ source env/bin/activate
$ pip install -r requirements.txt
```

## Running

```
$ ./main.py
```
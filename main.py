#!/usr/bin/env python3
import requests
import time
import logging
import os
from bs4 import BeautifulSoup
from urllib.parse import urlparse, urlunparse


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class CanonicalURL(object):
    def __init__(self, url_full: str, url_refr: str = None):
        self.url_full = url_full
        self.url_refr = CanonicalURL(str(url_refr)) if url_refr else None

        #
        # Parsing URL so it's possible to rebuild a canonical for comparison
        # Strips trailing slash and www. that would normally unique in each case
        #
        self.url_parsed = urlparse(url_full)
        self.domain_root = self.url_parsed.netloc.lstrip('www.')
        self.path_root = self.url_parsed.path.rstrip('/')
        self.url_canonical = urlunparse([
            '',
            self.domain_root,
            self.url_parsed.path.rstrip('/'),
            self.url_parsed.params,
            self.url_parsed.query,
            self.url_parsed.fragment
        ])

    def __str__(self):
        return self.url_canonical

    def __eq__(self, other):
        return self.url_canonical == other

    def is_external(self):
        if self.url_refr is None:
            return False

        return self.domain_root != self.url_refr.domain_root

    def create_path_relative(self, path: str):
        return urlunparse([
            self.url_parsed.scheme,
            self.url_parsed.netloc,
            path,
            '',
            '',
            ''
        ])

    def create_from_scheme(self, url: str):
        parsed = urlparse(url)
        return urlunparse([
            self.url_parsed.scheme,
            self.url_parsed.netloc,
            parsed.path,
            parsed.params,
            parsed.query,
            parsed.fragment
        ])


class Page(object):
    def __init__(self, url: CanonicalURL):
        self.url = url
        self.request = []
        self.headers = []
        self.anchors = []
        self.content = ''

    def fetch(self):
        headers = {
            'User-Agent': 'AlexBot (+http://alexphayes.co.uk)'
        }
        resp = requests.get(self.url.url_full, headers=headers)

        # Ordinarily, would handle errors
        # resp.raise_for_status()

        self.request = resp.request.headers
        self.headers = resp.headers
        self.history = resp.history
        self.content = resp.text
        
        bs = BeautifulSoup(self.content, 'html.parser')

        for anchor in bs.find_all('a'):
            # logger.debug('Found {}'.format(anchor))

            href = anchor.get('href', '')
            rel = anchor.get('rel', '')

            if not href:
                continue

            # Handle relative paths, ignore short scheme "//..."
            if not href.startswith('//') and href.startswith('/'):
                next_url = CanonicalURL(self.url.create_path_relative(href), self.url)

            # Handle short scheme "//..."
            elif href[:2] == '//':
                next_url = CanonicalURL(self.url.create_from_scheme(href), self.url)

            # Fully formed URL
            else:
                next_url = CanonicalURL(anchor['href'], self.url)
            
            # Prevent crawling linked sites
            if next_url.is_external():
                logger.debug('{} is external, will skip'.format(next_url))

            # Honoring robots
            elif 'nofollow' in rel:
                logger.debug('{} is nofollow, will skip'.format(next_url))

            # Success
            else:
                logger.debug('{} will follow'.format(next_url))
                self.anchors.append(next_url)

    def write_to_disk(self):
        data_path = os.path.abspath('./data')
        if not os.path.exists(data_path):
            os.makedirs(data_path)

        unique_name = self.url.url_canonical.replace('/', '_')

        create_lines = lambda d: '\n'.join(['{}: {}'.format(a, b) for a, b in d.items()])

        with open(os.path.join(data_path, unique_name), 'w') as f:
            f.write('\n\n'.join([
                self.url.url_full,
                self.url.url_refr.url_full if self.url.url_refr else '',
                create_lines(self.request),
                create_lines(self.headers),
                '\n'.join([repr(h) for h in self.history]),
                self.content
            ]))


class Crawler(object):
    def __init__(self, seed: str):
        self.record = []
        self.queue = [CanonicalURL(seed)]

    def _work(self):
        while len(self.queue):
            url = self.queue.pop()
            self.record.append(str(url))

            logger.info('** Fetch {}'.format(url))

            page = Page(url)
            page.fetch()
            page.write_to_disk()
            
            if page.anchors:
                self.queue.extend([
                    p for p in page.anchors
                    if str(p) not in self.record
                ])

            time.sleep(1)  # Be kind to site :)

    def run(self):
        self._work()


if __name__ == '__main__':
    crawler = Crawler('http://alexphayes.co.uk/')
    crawler.run()
